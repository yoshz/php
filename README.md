# yoshz/php

Base PHP Docker image build on top of `php:8.1-fpm-alpine` image.

Production version: `registry.gitlab.com/yoshz/php:8.1`
Development version: `registry.gitlab.com/yoshz/php:8.1-dev`

## Extensions
The following PHP extensions are preinstalled:
- amqp
- apcu
- intl
- pdo_mysql
- pdo_pgsql
- pcntl
- redis
- xsl
- zip

Additional extensions can be installed using the [docker-php-extension-installer](https://github.com/mlocati/docker-php-extension-installer):

```Dockerfile
RUN install-php-extensions gp
```

## Development version

The development version has the following preinstalled:

* composer
* php-cs-fixer
* local-php-security-checker
* xdebug
