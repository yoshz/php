php-image:
	docker pull registry.gitlab.com/yoshz/php:8.1
	docker build --pull -t registry.gitlab.com/yoshz/php:base --target base php-fpm
	docker run -it --rm registry.gitlab.com/yoshz/php:base ash

dev-image:
	docker pull registry.gitlab.com/yoshz/php:8.1-dev
	docker build --pull -t registry.gitlab.com/yoshz/php:dev --target dev php-fpm
	docker run -it --rm registry.gitlab.com/yoshz/php:dev bash

nginx-image:
	docker build --pull -t registry.gitlab.com/yoshz/php:nginx nginx
	docker run -it --rm registry.gitlab.com/yoshz/php:nginx
