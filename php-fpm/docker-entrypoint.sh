#!/bin/sh -e

docker_init_files() {
    local f
    for f; do
        case "$f" in
            *.sh)
                echo "$0: running $f"
                sh "$f"
                ;;
        esac
    done
}

docker_init_files /docker-entrypoint.d/*

if [ "${1#-}" != "$1" ]; then
    set -- php-fpm "$@"
fi

exec "$@"
